import React from 'react';
import { inject, observer } from 'mobx-react'
import Counter from '../components/Counter';
import Emoji from '../components/Emoji';
import Spinner from '../components/Spinner';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { Sparklines, SparklinesLine } from 'react-sparklines';
import { CSSTransitionGroup } from 'react-transition-group'
import abbreviate from 'number-abbreviate'
import './App.scss';


const Task = ({ task }) => (
	<a href={`https://getmakerlog.com/tasks/${task.id}`} className="Task">
		<div className="user">
			<img alt={task.user.username} src={task.user.avatar} />
		</div>
		<div className="content">
			{task.content}
		</div>
	</a>
)


@inject('stats')
@observer
class TotalCountTile extends React.Component {
	render() {
		return (
			<div className="tile">
				<h3 className="heading">
					All tasks
				</h3>
				<Counter value={this.props.stats.totalCount} />
				<Sparklines data={this.props.stats.totalTrend} >
    				<SparklinesLine color="rgba(255, 255, 255, 0.1)" />
				</Sparklines>
			</div>
		)
	}
}

@inject('stats')
@observer
class DoneCountTile extends React.Component {
	render() {
		return (
			<div className="tile">
				<h3 className="heading">
					Done tasks
				</h3>
				<Counter value={this.props.stats.doneCount} />
				<Sparklines data={this.props.stats.doneTrend} >
    				<SparklinesLine color="rgba(255, 255, 255, 0.1)" />
				</Sparklines>
			</div>
		)
	}
}

@inject('stats')
@observer
class RemainingCountTile extends React.Component {
	render() {
		return (
			<div className="tile">
				<h3 className="heading">
					Remaining tasks
				</h3>
				<Counter value={this.props.stats.remainingCount} />
				<Sparklines data={this.props.stats.remainingTrend} >
    				<SparklinesLine color="rgba(255, 255, 255, 0.1)" />
				</Sparklines>
			</div>
		)
	}
}

@inject('stats')
@observer
class UserCountTile extends React.Component {
	render() {
		return (
			<div className="tile">
				<h3 className="heading">
					Users
				</h3>
				<Counter value={this.props.stats.userCount} />
				<Sparklines data={this.props.stats.userTrend} >
    				<SparklinesLine color="rgba(255, 255, 255, 0.1)" />
				</Sparklines>
			</div>
		)
	}
}

@inject('stats')
@observer
class MrrTile extends React.Component {
	render() {
		return (
			<div className="tile">
				<h3 className="heading">
					MRR
				</h3>
				<Counter value={this.props.stats.mrr} />
				{this.props.stats.mrrTrend && (
				
				<Sparklines data={this.props.stats.mrrTrend} >
    				<SparklinesLine color="rgba(255, 255, 255, 0.1)" />
				</Sparklines>
				)}
			</div>
		)
	}
}

@inject('stats')
@observer
class StreakTile extends React.Component {
	render() {
		return (
			<div className="tile">
				<h3 className="heading">
					Highest streak
				</h3>
				<h3 className="Counter">
					<div>
						<Emoji emoji="🔥" /> {this.props.stats.streak}
					</div>
				</h3>
			</div>
		)
	}
}

@inject('stats')
@observer
class ProductsTile extends React.Component {
	render() {
		return (
			<div className="tile">
				<h3 className="heading">
					Products
				</h3>
				<h3 className="Counter">
					<div>
						{this.props.stats.products}
					</div>
				</h3>
			</div>
		)
	}
}


@inject('stats')
@observer
class LaunchesTile extends React.Component {
	render() {
		return (
			<div className="tile">
				<h3 className="heading">
					Launches
				</h3>
				<h3 className="Counter">
					<div>
						{this.props.stats.launches}
					</div>
				</h3>
			</div>
		)
	}
}


@inject('stats')
@observer
class PraiseTile extends React.Component {
	render() {
		return (
			<div className="tile">
				<h3 className="heading">
					Praise given
				</h3>
				<h3 className="Counter">
					<div>
						<Emoji emoji="👏" /> {abbreviate(this.props.stats.praise, 1)}
					</div>
				</h3>
			</div>
		)
	}
}

const TileGrid = props => (
	<div className="columns">
		<div className="column">
			<TotalCountTile />
			<DoneCountTile />
			<RemainingCountTile />
		</div>
		<div className="column">
			<UserCountTile />
			<MrrTile />
			<StreakTile />
		</div>
		<div className="column">
			<PraiseTile />
			<ProductsTile />
			<LaunchesTile />
		</div>
	</div>
)

@inject('stats')
@observer
class App extends React.Component {
	async componentDidMount() {
		await this.props.stats.loadOpenStats()
        this.props.stats.openSocket();
        setInterval(() => {
        	this.props.stats.loadOpenStats()
        }, 1000 * 60 * 60)
    }

    componentWillUnmount() {
        this.props.stats.closeSocket();
    }

	isReady = () => (
		this.props.stats.ready && !this.props.stats.failed
	)

	render() {
		const {
			loading,
		} = this.props.stats;

		console.log(this.props.stats.latestTasks)


		return (
			<div>
				<div className="header">
					<FontAwesomeIcon icon='check-circle' /> Open {loading && <span style={{display: 'inline-block'}}><Spinner small size={10} /></span>}	
				</div>
				<div className="ticker">
					<CSSTransitionGroup
			          transitionName="fade"
			          transitionEnterTimeout={500}
			          transitionLeaveTimeout={300}>
						{this.props.stats.latestTasks.map(t => <Task task={t} />)}
					</CSSTransitionGroup>
				</div>
				{this.isReady() ?
					<TileGrid />
					:
					<Spinner large />
				}
			</div>
		)
	}
}

export default App;
