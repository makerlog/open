import { observable, flow } from 'mobx'
import ReconnectingWebSocket from "reconnecting-websocket/dist/reconnecting-websocket";
import axios from '../lib/axios';



class StatsStore {
    @observable ready = false;
    @observable loading = true
    @observable totalCount = 0
    @observable doneCount = 0
    @observable remainingCount = 0
    @observable userCount = 0
    @observable userTrend = []
    @observable totalTrend = []
    @observable doneTrend = []
    @observable remainingTrend = []
    @observable mrr = []
    @observable mrrTrend = []
    @observable streak = 0
    @observable praise = 0
    @observable products = 0
    @observable launches = 0
    @observable latestTasks = []
    @observable failed = false

    loadOpenStats = flow(function* () {
        this.loading = true;
        this.failed = false;

        try {
            let stats = yield axios.get('https://api.getmakerlog.com/stats/open/');

            this.loading = false;
            this.ready = true;
            this.totalCount = stats.data.tasks_all
            this.doneCount = stats.data.tasks_done
            this.remainingCount = stats.data.tasks_all - stats.data.tasks_done

            this.userCount = stats.data.user_count;
            this.userTrend = Object.values(stats.data.new_users_per_day).reverse()


            this.mrr = stats.data.mrr;
            this.mrrTrend = stats.data.subs_per_day ? Object.values(stats.data.subs_per_day).reverse() : [];

            this.streak = stats.data.highest_streak;
            this.praise = stats.data.praise_given;
            this.products = stats.data.products_total;
            this.launches = stats.data.products_launched;

            this.doneTrend = Object.values(stats.data.done_per_day)
            this.remainingTrend = Object.values(stats.data.remaining_per_day)

            this.totalTrend = this.doneTrend.map((num, idx) => {
              return num + this.remainingTrend[idx];
            });

            this.failed = false
        } catch (e) {
            console.log(e)
            this.loading = false;
            this.failed = true;
        }
    })

    openSocket = (user) => {
        const socketUrl = 'wss://api.getmakerlog.com/explore/stream/';
        this.socket = new ReconnectingWebSocket(socketUrl);
        this.socket.onopen = () => {
            console.log("Makerlog: Tasks connection established.")
        };
        this.socket.onmessage = (event) => {
            const data = JSON.parse(event.data);
            console.log(`Makerlog: Event received through WS. (${data.type})`)
            switch(data.type) {
                case 'task.created':
                    if (data.payload.done) {
                        this.doneCount += 1
                    } else {
                        this.remainingCount += 1
                    }
                    this.totalCount += 1  
                    this.latestTasks = [...this.latestTasks, data.payload]
                    setTimeout(e => {
                        this.latestTasks = this.latestTasks.filter(t => t.id !== data.payload.id)
                    }, 6000)
                    break;

                case 'task.deleted':
                    if (data.payload.done) {
                        this.doneCount -= 1
                    } else {
                        this.remainingCount -= 1
                    }
                    this.totalCount -= 1  
                    break;

                default:
                    return;            
            }

        };
    }

    closeSocket = () => {
        this.socket.close()
    }

}

export const stats = new StatsStore();

