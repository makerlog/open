import React from 'react';
import PropTypes from 'prop-types';
import Emoji from 'components/Emoji';
import abbreviate from 'number-abbreviate'

const MakerScore = ({ score }) => (
    <span>
        <Emoji emoji={"🏆"} />&nbsp;{score ? abbreviate(score, 1) : 0}
    </span>
)

MakerScore.propTypes = {
    score: PropTypes.number.isRequired,
}

export default MakerScore;