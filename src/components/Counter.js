import React from 'react';
import Odometer from 'react-odometerjs';
import "./Counter.scss";

export default ({ value, large=false }) => (
	<div className={"Counter " + (large ? 'large' : '')}>
		<Odometer format="(,ddd)" value={value} />
	</div>
)